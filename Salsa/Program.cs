﻿using Microsoft.Extensions.Configuration;
using SixLabors.Fonts;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.Primitives;
using System;
using System.IO;

namespace Salsa
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

                IConfigurationRoot configuration = builder.Build();

                Image<Rgba32> image = Image.Load("input.png");
                var font = SystemFonts.CreateFont(configuration["Font"], 18, FontStyle.Regular);

                PointF pointF = new PointF(60, 170);

                image.Mutate(x => x.DrawText("Exp: " + DateTime.Now.AddDays(3).ToShortDateString(), font, Rgba32.Blue, pointF));

                image.Save("output.png");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.ToString());
            }
        }
    }
}
